package com.eduardorodriguez.planningpokereros

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import org.jetbrains.anko.find

class AddStoriesActivity : AppCompatActivity() {

    private lateinit var cancelAddingStories: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_stories)
        getUIReferences()
        setListeners()
    }

    private fun getUIReferences() {
        cancelAddingStories = find(R.id.cancelAddingStories)
    }

    private fun setListeners() {
        cancelAddingStories.setOnClickListener {
            startActivity(Intent(this, CreateRoomActivity::class.java))
        }
    }
}
