package com.eduardorodriguez.planningpokereros

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import org.jetbrains.anko.find

class MainActivity : AppCompatActivity() {

    private lateinit var createRoomActivityButton: Button
    private lateinit var joinRoomActivityButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getUIReferences()
        setListeners()
    }

    private fun getUIReferences() {
        createRoomActivityButton = find(R.id.createMainRoomButton)
        joinRoomActivityButton = find(R.id.joinButton)
    }

    private fun setListeners() {
        createRoomActivityButton.setOnClickListener {
            startActivity(Intent(this, CreateRoomActivity::class.java))
        }

        joinRoomActivityButton.setOnClickListener {
            startActivity(Intent(this, JoinRoomActivity::class.java))
        }
    }
}
