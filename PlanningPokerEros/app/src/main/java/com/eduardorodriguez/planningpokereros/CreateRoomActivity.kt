package com.eduardorodriguez.planningpokereros

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import org.jetbrains.anko.find

class CreateRoomActivity : AppCompatActivity() {

    private lateinit var createRoomActivityButton: Button
    private lateinit var cancelCreateRoomButton: Button
    private lateinit var typeSelectorSpinner: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_room)
        getUIReferences()
        setSpinnersContent()
        setListeners()
    }

    private fun getUIReferences() {
        createRoomActivityButton = find(R.id.createRoomButton)
        cancelCreateRoomButton = find(R.id.cancelCreateRoomButton)
        typeSelectorSpinner = find(R.id.typeRoomContent)
    }

    private fun setSpinnersContent() {
        ArrayAdapter.createFromResource(
            this,
            R.array.types_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            typeSelectorSpinner.adapter = adapter
        }
    }

    private fun setListeners() {
        createRoomActivityButton.setOnClickListener {
            startActivity(Intent(this, AddStoriesActivity::class.java))
        }

        cancelCreateRoomButton.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}
