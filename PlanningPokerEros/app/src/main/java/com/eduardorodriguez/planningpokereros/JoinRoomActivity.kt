package com.eduardorodriguez.planningpokereros

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import org.jetbrains.anko.find

class JoinRoomActivity : AppCompatActivity() {

    private lateinit var cancelJoinRoomButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join_room)
        getUIReferences()
        setListeners()
    }

    private fun getUIReferences() {
        cancelJoinRoomButton = find(R.id.cancelJoinRoomButton)
    }

    private fun setListeners() {
        cancelJoinRoomButton.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}
